/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.utils

/**
 * Converts [Int] to their hexadecimal representation, uppercase and with leading zeros
 */
fun hex(int: Int, leadingZeros: Int = 4) = Integer.toHexString(int).toUpperCase().padStart(leadingZeros, '0')

/**
 * Measures time in milliseconds
 * @return [Pair] at 0 index => elapsed time, measured from the [lambda] function
 * @return [Pair] at 1 index => return value of [lambda] function
 */
fun <T : Any?> measureTimeMillis(lambda: () -> T): Pair<Long, T> {
    val startTime = System.currentTimeMillis()
    val result = lambda.invoke()
    return Pair(System.currentTimeMillis() - startTime, result)
}

/**
 * Measures time in nanoseconds
 * @return [Pair] at 0 index => elapsed time, measured from the [lambda] function
 * @return [Pair] at 1 index => return value of [lambda] function
 */
fun <T : Any?> measureTimeNano(lambda: () -> T): Pair<Long, T> {
    val startTime = System.nanoTime()
    val result = lambda.invoke()
    return Pair(System.nanoTime() - startTime, result)
}