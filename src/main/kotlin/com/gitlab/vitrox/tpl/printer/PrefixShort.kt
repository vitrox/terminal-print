/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.Foreground
import com.gitlab.vitrox.tpl.escapeCode.General

/**
 * Represents short prefixes used by [Printer]
 */
object PrefixShort : Prefix {

    override val abort get() = PrefixData("A", Foreground.RED, General.BOLD)
    override val info get() = PrefixData("I", Foreground.BRIGHT_BLUE, General.BOLD)
    override val warn get() = PrefixData("W", Foreground.YELLOW, General.BOLD)
    override val debug get() = PrefixData("D", Foreground.BRIGHT_CYAN, General.BOLD)
    override val error get() = PrefixData("E", Foreground.BRIGHT_RED, General.BOLD)

}