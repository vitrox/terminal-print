/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.format
import com.gitlab.vitrox.tpl.escapeCode.formatf
import com.gitlab.vitrox.tpl.escapeCode.formatr
import com.gitlab.vitrox.tpl.escaped.escaped
import com.gitlab.vitrox.tpl.format.FormatState

fun warn(vararg any: Any) {
    Printer.warn(*any)
}

fun abort(vararg any: Any) {
    Printer.abort(*any)
}

fun info(vararg any: Any) {
    Printer.info(*any)
}

fun debug(vararg any: Any) {
    Printer.debug(*any)
}

fun error(vararg any: Any) {
    Printer.error(*any)
}

fun printe(exception: Exception) {
    Printer.printe(exception)
}

fun print(vararg any: Any) {
    FormatState.format().asPrint(any)
}

fun printf(vararg any: Any) {
    FormatState.formatf().asPrint(any)
}

fun printr(vararg any: Any) {
    FormatState.formatr().asPrint(any)
    println()
}

/**
 * Escapes [any] formatted with [format]
 */
fun escape(vararg any: Any) {
    println(format().asString(any).escaped())
}

/**
 * Escapes [any] formatted with [formatf]
 */
fun escapef(vararg any: Any) {
    println(formatf().asString(any).escaped())
}

/**
 * Escapes [any] formatted with [formatr]
 */
fun escaper(vararg any: Any) {
    println(formatr().asString(any).escaped())
}