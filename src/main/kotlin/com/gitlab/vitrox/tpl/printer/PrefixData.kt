/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.EscapeCode
import com.gitlab.vitrox.tpl.escapeCode.RESET
import com.gitlab.vitrox.tpl.format.Format

/**
 * Holds information about prefix formatting
 */
class PrefixData(name: String,
                 private val format: Format,
                 prefixBefore: String = PREFIX_BEFORE,
                 prefixAfter: String = PREFIX_AFTER,
                 multiline: String = MULTILINE) {

    constructor(name: String,
                vararg value: EscapeCode,
                prefixBefore: String = PREFIX_BEFORE,
                prefixAfter: String = PREFIX_AFTER,
                multiline: String = MULTILINE) :
            this(name, Format(*value), prefixBefore, prefixAfter, multiline)

    private fun color(string: () -> String): String = format.formatr(string.invoke()) + RESET

    /**
     * Default prefix values
     */
    companion object {
        const val PREFIX_BEFORE = "["
        const val PREFIX_AFTER = "] "
        const val MULTILINE = "» "
    }

    /**
     * Adds first line prefix to [string]
     */
    fun prefix(string: String) = prefix + string

    /**
     * Add prefix after first line to [string]
     */
    fun multiline(string: String) = multiline + string

    /**
     * Colored first line prefix as [String]
     */
    val prefix = color { prefixBefore + name + prefixAfter }

    /**
     * Colored prefix after first line as [String]
     */
    val multiline = color { multiline }

}