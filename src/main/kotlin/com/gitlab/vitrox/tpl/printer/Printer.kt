/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.RESET
import com.gitlab.vitrox.utils.measureTimeMillis
import kotlin.system.exitProcess

/**
 * Used to print text with prefix and concludes other neat print functions
 */
object Printer {

    private val defaultPrefix get() = object : Prefix {}
    private var prefix: Prefix = defaultPrefix

    private var debug = true

    /**
     * Formats [iterator] and puts a prefix before each [String]
     * Adds [RESET] after each line
     */
    fun printp(prefix: PrefixData, iterator: Iterator<Any>) {
        PrefixPrint.Normal(prefix).asPrint(iterator)
    }

    /**
     * Formats [iterable] and puts a prefix before each [String]
     * Adds [RESET] after each line
     */
    fun printp(prefix: PrefixData, iterable: Iterable<Any>) {
        printp(prefix, iterable.iterator())
    }

    /**
     * Prints each line of [any] with prefix
     */
    fun printp(prefix: PrefixData, vararg any: Any) {
        printp(prefix, listOf(*any))
    }

    /**
     * Disables debug messages, when [debug] is called no messages appear
     */
    fun disableDebug() {
        debug = false
    }

    /**
     * Enables debug messages
     * Used as default
     */
    fun enableDebug() {
        debug = true
    }

    /**
     * Changes default prefix messages
     */
    fun changePrefix(prefix: Prefix) {
        Printer.prefix = prefix
    }

    /**
     * Changes prefix back to default
     */
    fun defaultPrefix() {
        prefix = defaultPrefix
    }

    /**
     * Changes prefix to their short representation
     */
    fun shortPrefix() {
        prefix = PrefixShort
    }

    /**
     * Prints text into the terminal, used as debugging message
     * [disableDebug] to disable these messages
     */
    fun debug(vararg any: Any) {
        if (debug) printp(prefix.debug, *any)
    }

    /**
     * Prints text into the terminal, used as warning
     */
    fun warn(vararg any: Any) {
        printp(prefix.warn, *any)
    }

    /**
     * Prints text into the terminal, used by [quit]
     */
    fun abort(vararg any: Any) {
        printp(prefix.abort, *any)
    }

    /**
     * Prints text into the terminal, used to inform the user
     */
    fun info(vararg any: Any) {
        printp(prefix.info, *any)
    }

    /**
     * Prints text into the terminal, used for error messages
     */
    fun error(vararg any: Any) {
        printp(prefix.error, *any)
    }

    /**
     * Exits process and prints a [abort] message
     */
    fun quit(vararg any: Any, status: Int = 1) {
        abort(*any)
        exitProcess(status)
    }

    /**
     * Prints text into the terminal, used as debugging message
     * [disableDebug] to disable these messages
     */
    fun debug(iterable: Iterable<Any>) {
        if (debug) printp(prefix.debug, iterable)
    }

    /**
     * Prints text into the terminal, used as warning
     */
    fun warn(iterable: Iterable<Any>) {
        printp(prefix.warn, iterable)
    }

    /**
     * Prints text into the terminal, used by [quit]
     */
    fun abort(iterable: Iterable<Any>) {
        printp(prefix.abort, iterable)
    }

    /**
     * Prints text into the terminal, used to inform the user
     */
    fun info(iterable: Iterable<Any>) {
        printp(prefix.info, iterable)
    }

    /**
     * Prints text into the terminal, used for error messages
     */
    fun error(iterable: Iterable<Any>) {
        printp(prefix.error, iterable)
    }

    /**
     * Exits process and prints a [abort] message
     */
    fun quit(iterable: Iterable<Any>, status: Int = 1) {
        abort(iterable)
        exitProcess(status)
    }

    /**
     * Prints a [exception] as [error] message
     */
    fun printe(exception: Exception) {
        error(exception.toString(), *exception.stackTrace)
    }

    /**
     * Returns FormatInterface for debug messages
     */
    fun debug() = PrefixPrint.Normal(prefix.debug)

    /**
     * Returns FormatInterface for error messages
     */
    fun error() = PrefixPrint.Normal(prefix.error)

    /**
     * Returns FormatInterface for info messages
     */
    fun info() = PrefixPrint.Normal(prefix.info)

    /**
     * Returns FormatInterface for abort messages
     */
    fun abort() = PrefixPrint.Normal(prefix.abort)

    /**
     * Returns FormatInterface for warning messages
     */
    fun warn() = PrefixPrint.Normal(prefix.warn)

    /**
     * Catches any exception thrown by [run] and prints exception as error message
     * [fallback] is called when exception is thrown
     */
    fun <T : Any?> catchError(vararg any: Any, fallback: () -> T? = { null }, run: () -> T?): T? = try {
        run.invoke()
    } catch (exception: Exception) {
        printe(exception)
        if (any.isNotEmpty()) debug(*any)
        fallback.invoke()
    }

    /**
     * Catches any exception thrown by [run] and prints exception as error message
     * [fallback] is called when exception is thrown
     */
    fun <T : Any?> catchError(iterable: Iterable<Any>, fallback: () -> T? = { null }, run: () -> T?): T? = try {
        run.invoke()
    } catch (exception: Exception) {
        printe(exception)
        if (iterable.any()) debug(iterable)
        fallback.invoke()
    }

    /**
     * Used to measure time needed for running [lambda]
     * Prints measured time as debug messages
     * When [repeat] < 0 -> error message is thrown and [lambda] is called one time
     * When [repeat] = 0 -> total time is printed
     * When [repeat] > 0 -> total time, average time needed per [lambda], minimum and maximum times are printed
     */
    @JvmOverloads
    fun <T : Any?> measureTime(repeat: Int = 0,
                               name: String = "lambda function",
                               lambda: () -> T): T = when {
        repeat < 0 -> {
            error("Negative repeats are not allowed to measureTime from $name",
                    "Falling back to zero repeats...")
            measureTime(lambda = lambda)
        }
        repeat == 0 -> measureTimeMillis { lambda.invoke() }.apply {
            debug("Time measures for $name:", "Total time: $first ms")
        }.second

        else -> measureTimeMillis { lambda.invoke() }.apply {

            var sum = first
            var min = first
            var max = first

            for (i in 1..repeat) {
                with(kotlin.system.measureTimeMillis { lambda.invoke() }) {
                    sum += this
                    if (this < min) min = this
                    if (this > max) max = this
                }
            }
            debug("Time measures for $name with $repeat repeats:",
                    "Total time: $sum ms",
                    "Average time: ${sum / repeat} ms",
                    "Minimum time: $min ms",
                    "Maximum time: $max ms")
        }.second
    }
}
