/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.EscapeCode
import com.gitlab.vitrox.tpl.escapeCode.RESET
import com.gitlab.vitrox.tpl.format.*

sealed class PrefixPrint : FormatAbstract {

    class Normal(override val prefixData: PrefixData) : PrefixPrint(), FormatEnabled {

        override val format = Format()

        override fun <T, R> format(type: T,
                                   iterator: Iterator<*>,
                                   add: T.(Any?) -> Unit,
                                   iterable: (Iterator<*>) -> R,
                                   addIterable: T.(Any?) -> Unit): T = type.apply {
            for ((index, value) in iterator.withIndex()) {
                when (value) {
                    is EscapeCode -> escapeCode(value)
                    is Format -> formatLambda(value)
                    is Iterable<*> -> addIterable("${getPrefix(index)}${iterableLambda(iterable(value.iterator()))}")
                    else -> add("${getPrefix(index)}${elseLambda(value)}")
                }
            }
        }

        override fun iterable(): FormatInterface = FormatRow.Normal()

        override val iterableLambda: (Any?) -> Any? = {
            format.clearAll()
            super<PrefixPrint>.iterableLambda.invoke(it)
        }

        override val elseLambda: (Any?) -> String = {
            if (format.isEmpty()) it.toString()
            else format.toString().apply { format.clearAll() } + it + RESET
        }
    }

    class Disabled(override val prefixData: PrefixData) : PrefixPrint(), FormatDisabled {

        override fun iterable(): FormatInterface = FormatRow.Disabled

        override fun <T, R> format(type: T,
                                   iterator: Iterator<*>,
                                   add: T.(Any?) -> Unit,
                                   iterable: (Iterator<*>) -> R,
                                   addIterable: T.(Any?) -> Unit): T = type.apply {
            iterator.asSequence().filter {
                !(it is EscapeCode || it is Format)
            }.forEachIndexed { i, v ->
                when (v) {
                    is Iterable<*> -> addIterable("${getPrefix(i)}${iterableLambda(iterable(v.iterator()))}")
                    else -> add("${getPrefix(i)}${elseLambda(v)}")
                }
            }
        }
    }

    abstract val prefixData: PrefixData

    protected fun getPrefix(index: Int) = if (index == 0) prefixData.prefix else prefixData.multiline

}