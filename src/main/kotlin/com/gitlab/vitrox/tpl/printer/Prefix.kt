/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.printer

import com.gitlab.vitrox.tpl.escapeCode.Foreground
import com.gitlab.vitrox.tpl.escapeCode.General

/**
 * For prefix data used by [Printer]
 */
interface Prefix {

    val abort get() = PrefixData("ABORT", Foreground.RED, General.BOLD)
    val info get() = PrefixData("INFORMATION", Foreground.BRIGHT_BLUE, General.BOLD)
    val warn get() = PrefixData("WARNING", Foreground.YELLOW, General.BOLD)
    val debug get() = PrefixData("DEBUG", Foreground.BRIGHT_CYAN, General.BOLD)
    val error get() = PrefixData("ERROR", Foreground.BRIGHT_RED, General.BOLD)

}