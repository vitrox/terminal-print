/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.escapeCode

import com.gitlab.vitrox.tpl.format.FormatInterface
import com.gitlab.vitrox.tpl.format.FormatState

private const val SEPARATOR = ";"
private const val BEFORE = "\u001B["
private const val AFTER = "m"

/**
 * Creates a ANSI escape code out of their internal values
 */
fun join(iterable: Iterable<Short>) = BEFORE + iterable.joinToString(SEPARATOR) + AFTER

/**
 * Resets format back to default
 */
val RESET = BEFORE + General.DEFAULT.code + AFTER

/**
 * Calls toString() function for each non [EscapeCode] and Format
 * Reset ANSI escape codes to default at begin, end and after each string
 * That means [General.DEFAULT] can be completely omitted
 * Each of them represents a new line
 */
fun format(): FormatInterface = FormatState.format()

/**
 * Formats any non [EscapeCode] and Format with their toString() function
 * Add [RESET] to the string to disable background stretch on this line
 * Each of them represents a new line
 * Unlike [format] no reset add all
 * [General.DEFAULT] is needed to reset all formats
 * [Foreground.DEFAULT] to reset the [Foreground]
 * [Background.DEFAULT] to reset the [Background]
 */
fun formatf(): FormatInterface = FormatState.formatf()

/**
 * Formats any non [EscapeCode] and Format with their toString() function
 * Represents them in one line
 * Spaces between entries must be manually set
 */
fun formatr(): FormatInterface = FormatState.formatr()

/**
 * Formats any non [EscapeCode] and Format with their toString() function
 * Represents them in one line
 * Spaces between entries must be manually set
 * No automatically resets
 */
fun formatrf(): FormatInterface = FormatState.formatrf()