/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.builder

import com.gitlab.vitrox.tpl.escapeCode.format
import com.gitlab.vitrox.tpl.escaped.Escaped.asString
import com.gitlab.vitrox.tpl.format.Format
import com.gitlab.vitrox.tpl.format.FormatState
import com.gitlab.vitrox.tpl.printer.Printer.abort
import com.gitlab.vitrox.tpl.printer.Printer.debug
import com.gitlab.vitrox.tpl.printer.Printer.error
import com.gitlab.vitrox.tpl.printer.Printer.info
import com.gitlab.vitrox.tpl.printer.Printer.warn

/**
 * Main builder class
 */
open class PrintMessage {

    private val list = mutableListOf<Any>()

    /**
     * Add [String] to message
     */
    fun string(string: () -> String) {
        list.add(string.invoke())
    }

    /**
     * Build [Format] to add to message
     */
    open fun format(init: FormatBuilder.() -> Unit) {
        list.add(Format().apply { FormatBuilder(this).apply(init) })
    }

    /**
     * Escape [PrintMessage] included in [init]
     */
    fun escape(init: PrintMessage.() -> Unit) {
        addString(init, ::asString)
    }

    /**
     * Format [PrintMessage] freely
     */
    fun formatf(init: PrintMessage.() -> Unit) {
        list.add(FormatState.formatf().asString(PrintMessage().apply(init).getCollection()))
    }

    /**
     * Format [PrintMessage] as multirow
     */
    fun multirow(init: MultirowBuilder.() -> Unit) {
        list.add(MultirowBuilder().apply(init).toString())
    }

    /**
     * Adds debug message as [PrintMessage]
     */
    fun debug(init: PrintMessage.() -> Unit) {
        addString(init, debug()::asString)
    }

    /**
     * Adds error message as [PrintMessage]
     */
    fun error(init: PrintMessage.() -> Unit) {
        addString(init, error()::asString)
    }

    /**
     * Adds info message as [PrintMessage]
     */
    fun info(init: PrintMessage.() -> Unit) {
        addString(init, info()::asString)
    }

    /**
     * Adds abort message as [PrintMessage]
     */
    fun abort(init: PrintMessage.() -> Unit) {
        addString(init, abort()::asString)
    }

    /**
     * Adds warning message as [PrintMessage]
     */
    fun warn(init: PrintMessage.() -> Unit) {
        addString(init, warn()::asString)
    }

    /**
     * Adds custom prefix message as [PrintMessage]
     */
    fun prefix(name: String, init: PrefixBuilder.() -> Unit) {
        list.add(PrefixBuilder(name).apply(init).toString())
    }

    private inline fun addString(init: PrintMessage.() -> Unit, lambda: (Iterable<Any>) -> String) {
        list.add(lambda(PrintMessage().apply(init).getCollection()))
    }

    /**
     * Adds [Any] to list
     */
    fun add(init: () -> Any) {
        list.add(init.invoke())
    }

    override fun toString() = format().asString(list)

    /**
     * Get list as [Collection]
     */
    fun getCollection(): Collection<Any> = list

}