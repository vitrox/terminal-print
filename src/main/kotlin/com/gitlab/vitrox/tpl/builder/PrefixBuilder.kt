/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.builder

import com.gitlab.vitrox.tpl.format.Format
import com.gitlab.vitrox.tpl.printer.PrefixData
import com.gitlab.vitrox.tpl.printer.PrefixPrint

/**
 * Builder class for [PrefixData]
 */
class PrefixBuilder(private val name: String) : PrintMessage() {

    var before = PrefixData.PREFIX_BEFORE
    var after = PrefixData.PREFIX_AFTER
    var multiline = PrefixData.MULTILINE

    var format = Format()

    override fun format(init: FormatBuilder.() -> Unit) {
        format.apply { FormatBuilder(this).apply(init) }
    }

    /**
     * Get data holden by [PrefixBuilder] as [PrefixData]
     */
    fun prefixData() = PrefixData(name, format, before, after, multiline)

    override fun toString() = PrefixPrint.Normal(prefixData()).asString(getCollection())

}
