/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.builder

import com.gitlab.vitrox.tpl.escapeCode.Background
import com.gitlab.vitrox.tpl.escapeCode.Foreground
import com.gitlab.vitrox.tpl.escapeCode.General
import com.gitlab.vitrox.tpl.format.Format

/**
 * Builder class for [Format]
 */
class FormatBuilder(private val format: Format) {

    /**
     * Builder syntax for [Foreground]
     */
    fun foreground(foreground: () -> Foreground) {
        format.setForeground(foreground.invoke())
    }

    /**
     * Builder syntax for [Background]
     */
    fun background(background: () -> Background) {
        format.setBackground(background.invoke())
    }

    /**
     * Builder syntax for [General]
     */
    fun general(general: MutableSet<General>.() -> Unit) {
        format.addGeneral(mutableSetOf<General>().apply(general))
    }

}