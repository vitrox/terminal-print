/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.escapeCode.RESET

/**
 * Represents behavior for formatting one line (row)
 */
sealed class FormatRow : FormatAbstract {

    override val asPrintAdd: Unit.(Any?) -> Unit = { kotlin.io.print(it) }

    override val asStringAdd: StringBuilder.(Any?) -> Unit = { append(it) }

    class Normal : FormatRow(), FormatEnabled {

        override val format: Format = Format()

        override fun iterable(): FormatInterface = Normal()

        override val elseLambda: (Any?) -> String = {
            StringBuilder(2).also { s ->
                if (format.isNotEmpty()) {
                    s.append(format.toString() + it.toString())
                    if (!format.isGeneralDefault()) s.append(RESET)
                    format.clearAll()
                } else {
                    s.append(it.toString())
                }
            }.toString()
        }
    }

    class Free(override val format: Format = Format()) : FormatRow(), FormatEnabled {

        override fun iterable(): FormatInterface = Free(format)

        override val elseLambda: (Any?) -> String = {
            StringBuilder(2).also { s ->
                if (format.isNotEmpty()) {
                    s.append(format.toString())
                    if (format.isGeneralDefault()) format.clearAll()
                }
                s.append(it.toString())
            }.toString()
        }
    }

    object Disabled : FormatRow(), FormatDisabled {

        override fun iterable(): FormatInterface = Disabled

    }

}