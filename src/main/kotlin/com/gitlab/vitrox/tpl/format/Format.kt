/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.escapeCode.*

/**
 * Holds information about ANSI escape codes for formatting purposes
 */
class Format(iterator: Iterator<EscapeCode>) {

    private val general = linkedSetOf<Short>()
    private var foreground: Short? = null
    private var background: Short? = null

    constructor(vararg args: EscapeCode) : this(args.iterator())

    constructor(iterable: Iterable<EscapeCode>) : this(iterable.iterator())

    init {
        addEscapeCode(iterator)
    }

    override fun toString() =
            if (isNotEmpty())
                join(mutableListOf<Short>().apply {
                    addAll(general)
                    foreground?.let { add(it) }
                    background?.let { add(it) }
                })
            else ""

    /**
     * Adds [EscapeCode] to this [Format]
     * For [Foreground] and [Background] the last occurrence in [args] is used
     * Same occurrences of [General] are ignored
     */
    fun addEscapeCode(vararg args: EscapeCode) {
        addEscapeCode(args.iterator())
    }

    /**
     * Adds [EscapeCode] to this [Format]
     * For [Foreground] and [Background] the last occurrence in [iterator] is used
     * Same occurrences of [General] are ignored
     */
    fun addEscapeCode(iterator: Iterator<EscapeCode>) {
        iterator.forEach {
            when (it) {
                is General -> addGeneral(it)
                is Foreground -> setForeground(it)
                is Background -> setBackground(it)
            }
        }
    }

    /**
     * Adds [General] to the private [HashSet]
     * Ignores same occurrences of [General]
     */
    fun addGeneral(general: General) {
        this.general.add(general.code)
    }

    /**
     * Adds multiple [General]s to the private [HashSet]
     * Ignores same occurrences of [General]
     */
    fun addGeneral(general: Iterable<General>) {
        general.mapTo(this.general) { it.code }
    }

    /**
     * Overrides current [Foreground]
     */
    fun setForeground(foreground: Foreground) {
        this.foreground = foreground.code
    }


    /**
     * Overrides current [Background]
     */
    fun setBackground(background: Background) {
        this.background = background.code
    }

    /**
     * Builder syntax for [setBackground]
     */
    fun background(background: () -> Background) {
        setBackground(background.invoke())
    }

    /**
     * Removes [General] if found in the private [HashSet]
     */
    fun removeGeneral(general: General) {
        this.general.remove(general.code)
    }

    /**
     * Clears all [General] properties
     */
    fun clearGeneral() {
        general.clear()
    }

    /**
     * Replaces actual [Foreground] property with null
     */
    fun clearForeground() {
        foreground = null
    }

    /**
     * Replaces actual [Background] property with null
     */
    fun clearBackground() {
        background = null
    }

    /**
     * Clears all [General], [Foreground] and [Background] properties
     */
    fun clearAll() {
        clearGeneral()
        clearForeground()
        clearBackground()
    }

    /**
     * [+=] operator overloads
     * [General] is added to the private [HashSet]
     * If [Foreground] and [Background] properties from [other] are not null, this properties are overridden
     */
    operator fun plusAssign(other: Format) {
        general.addAll(other.general)
        other.foreground?.let { foreground = it }
        other.background?.let { background = it }
    }

    /**
     * Clears all properties and adds [General.DEFAULT] to the internal [HashSet]
     */
    fun default() {
        clearAll()
        addGeneral(General.DEFAULT)
    }

    /**
     * Checks if no properties are existing
     */
    fun isEmpty() = general.isEmpty() && foreground == null && background == null

    /**
     * Checks if properties are existing
     */
    fun isNotEmpty() = !isEmpty()

    /**
     * Check if [Format] is holding only [General.DEFAULT]
     */
    fun isGeneralDefault() = general.elementAtOrNull(0)?.let {
        return general.size == 1 && it == 0.toShort() && foreground == null && background == null
    } ?: false

    /**
     * Formats [any] with this [Format] in the first place
     */
    fun format(vararg any: Any): String = FormatState.format().asString(listOf(toString(), *any))

    /**
     * Formats [any] free without resets with this [Format] in the first place
     */
    fun formatf(vararg any: Any): String = FormatState.formatf().asString(listOf(toString(), *any))

    /**
     * Formats [any] only one line  with this [Format] in the first place
     */
    fun formatr(vararg any: Any): String = FormatState.formatr().asString(listOf(toString(), *any))

}