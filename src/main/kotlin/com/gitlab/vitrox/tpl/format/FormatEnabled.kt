/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.escapeCode.EscapeCode

/**
 * Represents behaviour when formatting is enabled
 */
interface FormatEnabled : FormatAbstract {

    val format: Format

    override fun <T, R> format(type: T,
                               iterator: Iterator<*>,
                               add: T.(Any?) -> Unit,
                               iterable: (Iterator<*>) -> R,
                               addIterable: T.(Any?) -> Unit): T = type.apply {
        iterator.forEach {
            when (it) {
                is EscapeCode -> escapeCode(it)
                is Format -> formatLambda(it)
                is Iterable<*> -> addIterable(iterable(it.iterator()).apply { iterableLambda(it) })
                else -> add(elseLambda(it))
            }
        }
    }

    val escapeCode: (EscapeCode) -> Unit get() = { format.addEscapeCode(it) }
    val formatLambda: (Format) -> Unit get() = { format += it }

}