/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

/**
 * Interface for formatting to get output as [String], [Collection] or printing to the console
 */
interface FormatInterface {

    /**
     * Prints output to console
     */
    fun asPrint(iterator: Iterator<*>)

    fun asPrint(array: Array<out Any>) {
        asPrint(array.iterator())
    }

    fun asPrint(iterable: Iterable<*>) {
        asPrint(iterable.iterator())
    }

    /**
     * Get formatted input as [String]
     */
    fun asString(iterator: Iterator<*>): String

    fun asString(array: Array<out Any>) = asString(array.iterator())

    fun asString(iterable: Iterable<*>) = asString(iterable.iterator())

    /**
     * Get formatted input as [Collection]
     */
    fun asCollection(iterator: Iterator<*>): Collection<String>

    fun asCollection(array: Array<out Any>) = asCollection(array.iterator())

    fun asCollection(iterable: Iterable<*>) = asCollection(iterable.iterator())

}