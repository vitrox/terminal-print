/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

/**
 * Default implementations for formatting
 */
interface FormatAbstract : FormatInterface {

    fun <T, R> format(type: T,
                      iterator: Iterator<*>,
                      add: T.(Any?) -> Unit,
                      iterable: (Iterator<*>) -> R,
                      addIterable: T.(Any?) -> Unit = add): T

    override fun asPrint(iterator: Iterator<*>) {
        format(Unit, iterator, ::asPrintAdd.get(), iterable()::asPrint, {})
    }

    override fun asString(iterator: Iterator<*>): String
            = format(StringBuilder(), iterator, ::asStringAdd.get(), iterable()::asString).toString()

    override fun asCollection(iterator: Iterator<*>): Collection<String>
            = format(mutableListOf(), iterator, ::asCollectionAdd.get(), iterable()::asCollection)

    fun iterable(): FormatInterface
    val elseLambda: (Any?) -> String

    val iterableLambda: (Any?) -> (Any?)
        get() = {}

    val asPrintAdd: Unit.(Any?) -> Unit
        get() = { kotlin.io.println(it) }

    val asStringAdd: StringBuilder.(Any?) -> Unit
        get() = { appendln(it) }

    val asCollectionAdd: MutableList<String>.(Any?) -> Unit
        get() = { add(it.toString()) }

}