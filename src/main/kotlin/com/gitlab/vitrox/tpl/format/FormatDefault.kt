/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.escapeCode.EscapeCode
import com.gitlab.vitrox.tpl.escapeCode.General
import com.gitlab.vitrox.tpl.escapeCode.RESET

/**
 * Represents default formatting behaviour for multiline's
 */
sealed class FormatDefault : FormatAbstract {

    class Normal : FormatDefault(), FormatEnabled {

        override val format = Format(General.DEFAULT)

        override fun iterable(): FormatInterface = FormatRow.Normal()

        override val iterableLambda: (Any?) -> Any? = {
            format.clearAll()
            super<FormatDefault>.iterableLambda.invoke(it)
        }

        override val elseLambda: (Any?) -> String = {
            if (format.isEmpty()) it.toString()
            else format.toString().apply { format.clearAll() } + it + RESET
        }

    }

    class Free(override val format: Format = Format()) : FormatDefault(), FormatEnabled {

        private var lastString = true

        override val escapeCode: (EscapeCode) -> Unit = {
            if (it == General.DEFAULT) format.clearAll()
            super.escapeCode(it)
            lastString = false
        }

        override val formatLambda: (Format) -> Unit = {
            super.formatLambda(it)
            lastString = false
        }

        override fun iterable(): FormatInterface = FormatRow.Free(format)

        override val iterableLambda: (Any?) -> Any? = {
            lastString = true
            super<FormatDefault>.iterableLambda.invoke(it)
        }

        override val elseLambda: (Any?) -> String = {
            if (lastString) it.toString()
            else format.toString().apply { lastString = true } + it
        }

    }

    object Disabled : FormatDefault(), FormatDisabled {

        override fun iterable(): FormatInterface = Disabled

    }

}