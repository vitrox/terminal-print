/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.printer.Printer
import com.gitlab.vitrox.tpl.printer.debug

/**
 * Behavior for enabled and disabled ANSI escape codes
 */
sealed class FormatState {

    /**
     * Activates and deactivates ANSI escape codes based on operating system
     * Supports operating system over Windows 10 and other
     */
    companion object : FormatState() {

        override fun format(): FormatInterface = state.format()

        override fun formatf(): FormatInterface = state.formatf()

        override fun formatr(): FormatInterface = state.formatr()

        override fun formatrf(): FormatInterface = state.formatrf()

        private const val DISABLED_ESCAPE_CODE = "FormatDisabled escape codes, because they are not supported by"

        /**
         * Changes [FormatState] on initialize based on the operating system
         * For Windows operating systems, only version above 10 is supported
         */
        private var state = Printer.catchError("$DISABLED_ESCAPE_CODE your os.") {
            val os = System.getProperty("os.name")
            if (os.contains("Windows", true) && !os.contains("Windows 10", false))
                Disabled.apply { debug("$DISABLED_ESCAPE_CODE $os.") }
            else Enabled
        } ?: Disabled

        /**
         * Enables ANSI escape codes
         * Make sure there are supported by your operating system or this could lead to unintentional behavior
         */
        fun enable() {
            state = Enabled
        }

        /**
         * Disables ANSI escape codes
         */
        fun disable() {
            state = Disabled
        }

    }

    /**
     * Represents behavior when ANSI escape codes are enabled
     */
    private object Enabled : FormatState() {

        override fun format(): FormatInterface = FormatDefault.Normal()

        override fun formatf(): FormatInterface = FormatDefault.Free()

        override fun formatr(): FormatInterface = FormatRow.Normal()

        override fun formatrf(): FormatInterface = FormatRow.Free()

    }

    /**
     * Represents behavior when ANSI escape codes are disabled
     */
    private object Disabled : FormatState() {

        override fun format() = FormatDefault.Disabled

        override fun formatf() = FormatDefault.Disabled

        override fun formatr() = FormatRow.Disabled

        override fun formatrf() = FormatRow.Disabled

    }

    abstract fun format(): FormatInterface
    abstract fun formatf(): FormatInterface
    abstract fun formatr(): FormatInterface
    abstract fun formatrf(): FormatInterface

}
