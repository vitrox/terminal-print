/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.format

import com.gitlab.vitrox.tpl.escapeCode.EscapeCode

/**
 * Represents behaviour when formatting is disabled
 */
interface FormatDisabled : FormatAbstract {

    override fun <T, R> format(type: T,
                               iterator: Iterator<*>,
                               add: T.(Any?) -> Unit,
                               iterable: (Iterator<*>) -> R,
                               addIterable: T.(Any?) -> Unit): T = type.apply {
        iterator.asSequence().filter {
            !(it is EscapeCode || it is Format)
        }.forEach {
            when (it) {
                is Iterable<*> -> addIterable(iterableLambda(iterable(it.iterator())))
                else -> add(elseLambda(it))
            }
        }
    }

    override val elseLambda: (Any?) -> String get() = { it.toString() }

}