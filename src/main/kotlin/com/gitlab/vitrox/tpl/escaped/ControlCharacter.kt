/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.escaped

/**
 * Holds information about control character replaced by [Escaped]
 */
enum class ControlCharacter(val dec: Int, val replace: Char) {
    NULL(0, '0'),
    BELL(7, 'a'),
    BACKSPACE(8, 'b'),
    HORIZONTAL_TAB(9, 't'),
    LINE_FEED(10, 'n'),
    VERTICAL_TAB(11, 'v'),
    FORM_FEED(12, 'f'),
    CARRIAGE_RETURN(13, 'r');

    /**
     * Static class with [table] to get [ControlCharacter] values as [Array]
     */
    companion object {
        val table by lazy { ControlCharacter.values() }
    }

}