/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.gitlab.vitrox.tpl.escaped

import com.gitlab.vitrox.tpl.format.FormatInterface

/**
 * Represents behaviour for escaping
 */
object Escaped: FormatInterface {

    /**
     * Replaces characters with their unicode representation
     * For each [any] toString() is called
     * @return each [any] escaped as [String]
     */
    private fun <T> escape(type: T,
                           iterator: Iterator<*>,
                           add: T.(String) -> Unit): T
        = type.apply {
        iterator.forEach {
            add(escapeString(it.toString()))
        }
    }

    fun escapeString(string: String)
     = StringBuilder().apply {
        string.forEach {
            with(it.toInt()) {
                append(if (this in 0..31 || this == 127)
                    "\\${com.gitlab.vitrox.tpl.escaped.ControlCharacter.table.find { it.dec == this }?.replace
                            ?: "u${com.gitlab.vitrox.utils.hex(this)}"}" else it.toString())
            }
        }
    }.toString()

    override fun asPrint(iterator: Iterator<*>) {
        escape(Unit, iterator) {
            println(it)
        }
    }


    override fun asString(iterator: Iterator<*>): String
        = escape(StringBuilder(), iterator) {
        append(it)
    }.toString()


    override fun asCollection(iterator: Iterator<*>): Collection<String>
        = escape(mutableListOf(), iterator) {
        add(it)
    }

}