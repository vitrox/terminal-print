# Terminal print library (TPL) 

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Release](https://jitpack.io/v/com.gitlab.Vitrox/terminal-print.svg)]
(https://jitpack.io/#com.gitlab.vitrox/terminal-print)

Small library to output colored text into the terminal
using [ANSI escape codes](https://wikipedia.org/wiki/ANSI_escape_code).

Supports [Windows 10 and above](https://docs.microsoft.com/windows/console/console-virtual-terminal-sequences),
and any other operating system which supports [ANSI escape codes](https://wikipedia.org/wiki/ANSI_escape_code).

### Features

+ Output colored text into the terminal
    + For one or multiple lines
    + Save format as variable for reuse
    + With builder syntax
+ Neat colored build-in prefixes for debug, information, warning, error and abort messages
    + With long and short variances
    + And option to use own prefixes
    + Toggle debug messages
+ Escape (replace) any non [ASCII (basic latin)](https://www.rapidtables.com/code/text/ascii-table.html) character
 (between inclusive 32 to 126 of their decimal representation) as [UTF-8](https://wikipedia.org/wiki/UTF-8)
+ Utility (utils) contains:
    + Decimal number to hexadecimal representation, uppercase with leading zeros
    + Measures time (nano or milliseconds) and returns a [Pair](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-pair/index.html)
    with the needed time and return value of the lambda

### [How to install?](https://jitpack.io/#com.gitlab.vitrox/terminal-print)

### Usage

See [example/kotlin](example/kotlin) and [source code](src/main/kotlin/com/gitlab/vitrox).



