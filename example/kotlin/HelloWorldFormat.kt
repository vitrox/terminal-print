/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.gitlab.vitrox.tpl.builder.print
import com.gitlab.vitrox.tpl.printer.Printer
import com.gitlab.vitrox.tpl.printer.print
import com.gitlab.vitrox.tpl.printer.printr

fun main(args: Array<String>) {

    Printer.measureTime {

        /**
         * Hello
         * World!
         */
        print(helloFormat, "Hello",
                worldFormat, "World!")

        /**
         * Hello World!
         */
        printr(helloFormat, "Hello",
                " ",
                worldFormat, "World!")

        /**
         * Builder syntax
         * Hello
         * World!
         */
        print {
            add {
                helloFormat
            }
            string { "Hello" }
            add {
                worldFormat
            }
            string { "World!" }
        }
    }

    /**
     * Builder syntax
     * Hello World!
     */
    print {
        multirow {
            separator = " "
            add {
                helloFormat
            }
            string { "Hello" }
            add {
                worldFormat
            }
            string { "World!" }
        }
    }

    print {

        prefix("Test") {

            string { "Hello" }
            string { "World" }

        }

    }

}

