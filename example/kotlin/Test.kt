/**
 * terminal-print
 * Copyright (C) 2018  Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import com.gitlab.vitrox.tpl.escapeCode.Background
import com.gitlab.vitrox.tpl.escapeCode.Foreground
import com.gitlab.vitrox.tpl.format.Format

/**
 * Resources for examples
 */

val helloFormat = Format(Foreground.RED, Background.BRIGHT_CYAN)
val worldFormat = Format(Foreground.BRIGHT_MAGENTA, Background.BRIGHT_BLACK)

object asEscapeCode {

    val helloWorldMultiline = arrayOf(Foreground.RED, Background.BRIGHT_CYAN, "Hello",
            Foreground.BRIGHT_MAGENTA, Background.BRIGHT_BLACK, "World!")

    val helloWorldRow = arrayOf(Foreground.RED, Background.BRIGHT_CYAN, "Hello", " ",
            Foreground.BRIGHT_MAGENTA, Background.BRIGHT_BLACK, "World!")

}

object asFormat {

    val helloWorldMultiline = arrayOf(helloFormat, "Hello",
            worldFormat, "World!")

    val helloWorldRow = arrayOf(helloFormat, "Hello", " ",
            worldFormat, "World!")

}
