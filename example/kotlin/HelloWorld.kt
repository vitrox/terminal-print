/**
 * terminal-print
 * Copyright (C) 2018 Matthias Schwarz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import com.gitlab.vitrox.tpl.builder.print
import com.gitlab.vitrox.tpl.escapeCode.Background
import com.gitlab.vitrox.tpl.escapeCode.Foreground
import com.gitlab.vitrox.tpl.format.FormatState
import com.gitlab.vitrox.tpl.printer.Printer
import com.gitlab.vitrox.tpl.printer.print
import com.gitlab.vitrox.tpl.printer.printf
import com.gitlab.vitrox.tpl.printer.printr

fun main(args: Array<String>) {

    /**
     * To disable formatting
     */
    //FormatState.disable()

    /**
     * Hello
     * World!
     */
    print(*asEscapeCode.helloWorldMultiline)

    /**
     * Hello World!
     */
    printr(*asEscapeCode.helloWorldRow)

    /**
     * Hello
     * World!
     */
    printf(*asEscapeCode.helloWorldMultiline)

    /**
     * Builder syntax
     * Hello
     * World!
     */
    print {
        format {
            foreground { Foreground.RED }
            background { Background.BRIGHT_CYAN }
        }
        string { "Hello" }
        format {
            foreground { Foreground.BRIGHT_MAGENTA }
            background { Background.BRIGHT_BLACK }
        }
        string { "World!" }
    }

    /**
     * Builder syntax
     * Hello
     * World!
     */
    print {
        add {
            helloFormat
        }
        string { "Hello" }
        add {
            worldFormat
        }
        string { "World!" }
    }

    /**
     * Builder syntax
     * Hello World!
     */
    print {
        multirow {
            separator = " "
            format {
                foreground { Foreground.RED }
                background { Background.BRIGHT_CYAN }
            }
            string { "Hello" }
            format {
                foreground { Foreground.BRIGHT_MAGENTA }
                background { Background.BRIGHT_BLACK }
            }
            string { "World!" }
        }
    }

    print {
        escape {
            multirow {
                separator = " "
                format {
                    foreground { Foreground.RED }
                    background { Background.BRIGHT_CYAN }
                }
                string { "Hello" }
                format {
                    foreground { Foreground.BRIGHT_MAGENTA }
                    background { Background.BRIGHT_BLACK }
                }
                string { "World!" }
            }
        }
    }

    Printer.measureTime {
        print(Background.BRIGHT_BLACK, "Batz", listOf(Foreground.RED, Background.BRIGHT_CYAN, "Hello",
                Foreground.BRIGHT_MAGENTA, Background.BRIGHT_BLACK, listOf("Fuzz", "Buzz"), "World!"), "Fazz")
    }

}

